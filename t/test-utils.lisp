(defpackage #:daily-challenges-test/test-utils
  (:use #:cl)
  (:nicknames #:test-utils)
  (:export #:with-test-input))

(in-package #:test-utils)

(defmacro with-test-input (instring &body body)
  `(let ((*standard-output* (make-string-output-stream))
         (*standard-input* (make-string-input-stream (format nil ,instring))))
     ,@body))
