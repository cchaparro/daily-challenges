#!/usr/bin/env bash

set -e

markdown_location=`which markdown | cut -d: -f2`
if [ -z "$markdown_location" ]; then
    apt-get update
    apt-get install -y discount
fi

cd doc/

for f in `ls`; do
    markdown -o "${f%.*}.html" "$f"
done

cd ..

tar czf daily-challenges.tar.gz coverage/ doc/*.html
