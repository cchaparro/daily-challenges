#!/usr/bin/env bash

set -e

source_registry=${source_registry:-/etc/common-lisp/source-registry.conf.d/}
mkdir -p $source_registry
echo "(:tree \"`pwd`\")" > $source_registry/local.conf
