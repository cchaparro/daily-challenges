#-split-sequence
(ql:quickload "split-sequence")

(let ((loaded? (asdf:load-system "daily-challenges")))
  (format t "Loading ~:[failed~;successful~]!~%" loaded?))
