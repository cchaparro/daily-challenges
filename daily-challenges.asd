(defsystem #:daily-challenges
  :description "Solutions to challenges from DailyCodingProblem.com written in Common Lisp."
  :version (:read-file-form "variables.lisp" :at (0 1))
  :author "Cameron V Chaparro"
  :license "GPL3"
  :depends-on (#:split-sequence)
  :components ((:module "src"
                :components ((:file "utils")
                             (:file "01" :depends-on ("utils")))))
  :in-order-to ((test-op (test-op #:daily-challenges-test))))
