#-prove
(ql:quickload "prove")

#+swank
(setf prove:*enable-colors* nil)

(defsystem #:daily-challenges-test
  :description "Test system for daily-challenges."
  :depends-on (#:daily-challenges
               #:prove)
  :components ((:module "t"
                :components ((:file "test-utils")
                             (:file "01-tests" :depends-on ("test-utils")))))
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run) :prove) c)))
