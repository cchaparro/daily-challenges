# daily-challenges

Solutions to challenges from DailyCodingProblem.com written in Common Lisp.

# Requirements

- SBCL v1.5.3

# License

Copyleft (ↄ) 2019, Cameron Chaparro.

This work is licensed under the terms of the GPL3.
