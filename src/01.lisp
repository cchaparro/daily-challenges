(defpackage #:daily-challenges/01
  (:use #:cl
        #:utils)
  (:export #:main
           #:any-sum-p))

(in-package #:daily-challenges/01)

(defun main ()
  "Prompts the user for a list of numbers and then a single number.  Returns T if any two numbers in the list add to the
single number; otherwise returns NIL."
  (let ((l (get-numbers-from-user))
        (k (get-number-from-user)))
    (any-sum-p l k)))

(defun any-sum-p (l k)
  "Returns T if any two numbers from L sum to the value of K; otherwise, NIL."
  (loop :for element :in l
        :when (member t (elements-sum-to-k-p element (remove element l) k))
          :do (return t)))

(defun elements-sum-to-k-p (element l k)
  "Returns a list containing either T or NIL at each index.  If the sum of ELEMENT and each item in the list are equal
to K, then the new list will contain a T at that position; otherwise, it will contain a NIL at that position."
  (mapcar (lambda (e)
            (= (+ e element) k))
          l))
